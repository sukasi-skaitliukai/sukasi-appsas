package harambo.sukasiskaitliukai;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class TestActivity extends AppCompatActivity {


    static final int REQUEST_IMAGE_CAPTURE = 1;
    public ImageView mImageView;
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        queue = Volley.newRequestQueue(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                dispatchTakePictureIntent();

            }

        });

        mImageView = (ImageView) findViewById(R.id.mImageView);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 90, baos);
        byte[] b = baos.toByteArray();
        //String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return null;//imageEncoded;
    }

    public static Bitmap decodeBase64(String input) {
        //byte[] decodedByte = Base64.decode(input, 0);
        return null; //BitmapFactory.decodeByteArray(decodedByte, 0,      decodedByte.length);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)







    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {





            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    String urlString =  "http://stackoverflow.com";

                    Bundle extras = data.getExtras();
                    final Bitmap imageBitmap = (Bitmap) extras.get("data");

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    String image = Base64.getEncoder().encodeToString(byteArray);


                    JSONObject jsonObj = new JSONObject();
                    try {
                        // adding some keys
                        //JSONObject header = new JSONObject();
                        //header.put("Accept", "application/json");
                        //header.put("Content-Type", "application/json");
                        //jsonObj.put("header", header);
                        jsonObj.put("image", image);

                    } catch (JSONException ex) {
                        //display.setText("Error Occurred while building JSON");
                        ex.printStackTrace();
                    }


                    //jsonObj.toString(); //data to post

                    URL url = null;
                    try {
                        url = new URL("http://213.159.53.187:8080/rs/process");
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }


                    HttpURLConnection conn = null;
                    try {
                        conn = (HttpURLConnection) url.openConnection();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        conn.setRequestMethod("POST");
                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    }
                    conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    //TODO: add date
                    //Date c = Calendar.getInstance().getTime();
                    //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMANY);
                    //String formattedDate = df.format(c);

                    //TestData testData = new TestData(input.getText().toString(), mSelectedScanner.getId(), "Android " + Build.VERSION.RELEASE, formattedDate, mScenarioStatuses.get(mSelectedIN));
                    //Gson gson = new Gson();
                    String stringJson = jsonObj.toString();

                    DataOutputStream os = null;
                    try {
                        os = new DataOutputStream(conn.getOutputStream());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        os.writeBytes(stringJson);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        os.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        os.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        System.out.println("STATUS" + String.valueOf(conn.getResponseCode()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        System.out.println("MSG" + conn.getResponseMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                    try {


                        InputStream  is = conn.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                        StringBuilder sb = new StringBuilder();

                        String line = null;
                        try {
                            while ((line = reader.readLine()) != null) {
                                sb.append(line + "\n");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        String response = sb.toString();
                        is.close();
                        System.out.println("RESP" + response );

                        JSONObject respJson = new JSONObject(response);

                        System.out.println("BBBBBBBBBBBBBBB" + respJson.getString("image"));

                        byte[] imageArray = Base64.getDecoder().decode(new String(respJson.getString("image")).getBytes("UTF-8"));

                        Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0, imageArray.length);

                        mImageView.setImageBitmap(bmp);

                        conn.disconnect();

                    }catch (Exception e){}

                    System.out.println("JSON" + "[" + stringJson + "]");

                }
            });

            thread.start();

            //new PostRequest().execute(url, jsonObj);

        }
    }

    //@Override   not working !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    protected void onActivityResults(int requestCode, int resultCode, Intent data) {
        System.setProperty("http.proxyHost", "proxy.example.com");
        System.setProperty("http.proxyPort", "8080");
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //final JSONObject jsonBody = new JSONObject();
            //String url = "http://213.159.53.187:8080/rs/process";
            URL url = null;
            try {
                url = new URL("http://stackoverflow.com");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Bundle extras = data.getExtras();
            final Bitmap imageBitmap = (Bitmap) extras.get("data");

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            //final String image = Base64.getEncoder().encodeToString(byteArray);



            final JSONObject jsonBody = new JSONObject();
            try {
                // adding some keys
                JSONObject header = new JSONObject();
                header.put("Accept", "application/json");
                header.put("Content-Type", "application/json");
                jsonBody.put("header", header);
                //jsonBody.put("image", image);

            } catch (JSONException ex) {
                //display.setText("Error Occurred while building JSON");
                ex.printStackTrace();
            }


            try {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);

                //TODO: add date
                //Date c = Calendar.getInstance().getTime();
                //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMANY);
                //String formattedDate = df.format(c);

                //TestData testData = new TestData(input.getText().toString(), mSelectedScanner.getId(), "Android " + Build.VERSION.RELEASE, formattedDate, mScenarioStatuses.get(mSelectedIN));
                //Gson gson = new Gson();
                String stringJson = jsonBody.toString();

                System.out.println("JSON" + "[" + stringJson + "]");
                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                os.writeBytes("[" + stringJson + "]");

                os.flush();
                os.close();

                System.out.println("STATUS" + String.valueOf(conn.getResponseCode()));
                System.out.println("MSG" + conn.getResponseMessage());

                conn.disconnect();

                //makeToast("Successfully uploaded to server");

            }
            catch (Exception e){
                System.out.println("Exception: " + e.getMessage());
            }

            //String image = encodeTobase64(imageBitmap);

            //final JSONObject jsonBody = new JSONObject("{\"type\":\"example\"}");
            /*
            final JSONObject jsonBody = new JSONObject();
            try {
                // adding some keys
                JSONObject header = new JSONObject();
                //header.put("Accept", "application/json");
                //header.put("Content-Type", "application/json");
                //jsonObject.put("header", header);
                jsonBody.put("image", image);

            } catch (JSONException ex) {
                //display.setText("Error Occurred while building JSON");
                ex.printStackTrace();
            }
            System.out.println(jsonBody.toString());


            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            System.out.println("Response" + response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("Response" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("image", image);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
            */ //////////////////////////////////////////////////////////////////////////////////////////////////

        /*
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //mTxtDisplay.setText("Response: " + response.toString());
                        System.out.println(response.toString());
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        System.out.println(error.toString());

                    }


                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }

        };
        System.out.println("request: " + jsObjRequest.toString());
        queue.add(jsObjRequest);
        */
        }
    }

    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            final Bitmap imageBitmap = (Bitmap) extras.get("data");
            try {
                // Create HTTP objects

                RequestQueue queue = Volley.newRequestQueue(this);
                String url = "http://213.159.53.187:8080/rs/process";
                String image = encodeTobase64(imageBitmap);
                final String postMessage = "{\"image\":\""+image+"\""+"}";
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                System.out.println(s);
                                //Disimissing the progress dialog
                                //loading.dismiss();
                                //Showing toast message of the response
                                //Toast.makeText(MainActivity.this, s , Toast.LENGTH_LONG).show();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                //Dismissing the progress dialog
                                //loading.dismiss();
                                System.out.println(volleyError.toString());
                                //Showing toast
                                //Toast.makeText(MainActivity.this, ""+volleyError, Toast.LENGTH_LONG).show();
                            }
                        }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        //Converting Bitmap to String
                        String image = encodeTobase64(imageBitmap);
                        //Creating parameters
                        Map<String,String> params = new Hashtable<String, String>();
                        params.put("image", postMessage);
                        //returning parameters
                        return params;
                    }

                };
                queue.add(stringRequest);

                /*
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Display the first 500 characters of the response string.
                                mTextView.setText("Response is: " + response.substring(0, 500));
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //mTextView.setText("That didn't work!");
                    }
                });
                */
                // Add the request to the RequestQueue.
                //queue.add(stringRequest);

/*
            } catch (Exception e) {
                e.printStackTrace();
            }
            //mImageView.setImageBitmap(imageBitmap);
        }
    }
    */
}

class PostRequest extends AsyncTask<URL, Integer, Long> {

    private Integer progressPercent;

    protected void doInBackground(URL url, JSONObject jsonObject) throws IOException {

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);
        conn.setDoInput(true);

        //TODO: add date
        //Date c = Calendar.getInstance().getTime();
        //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMANY);
        //String formattedDate = df.format(c);

        //TestData testData = new TestData(input.getText().toString(), mSelectedScanner.getId(), "Android " + Build.VERSION.RELEASE, formattedDate, mScenarioStatuses.get(mSelectedIN));
        //Gson gson = new Gson();
        String stringJson = jsonObject.toString();

        System.out.println("JSON" + "[" + stringJson + "]");
        DataOutputStream os = new DataOutputStream(conn.getOutputStream());
        os.writeBytes("[" + stringJson + "]");

        os.flush();
        os.close();

        System.out.println("STATUS" + String.valueOf(conn.getResponseCode()));
        System.out.println("MSG" + conn.getResponseMessage());

        conn.disconnect();

        //makeToast("Successfully uploaded to server");
    }

    protected void onProgressUpdate(Integer... progress) {
        setProgressPercent(progress[0]);
    }

    @Override
    protected Long doInBackground(URL... urls) {
        return null;
    }

    protected void onPostExecute(Long result) {
        showDialog("Downloaded " + result + " bytes");
    }

    private void showDialog(String s) {
    }

    public void setProgressPercent(Integer progressPercent) {
        this.progressPercent = progressPercent;
    }
}
